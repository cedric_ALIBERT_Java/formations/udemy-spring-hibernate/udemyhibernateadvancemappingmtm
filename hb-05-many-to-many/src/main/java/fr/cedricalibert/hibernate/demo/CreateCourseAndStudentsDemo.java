package fr.cedricalibert.hibernate.demo;


import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import fr.cedricalibert.hibernate.demo.entity.Course;
import fr.cedricalibert.hibernate.demo.entity.Instructor;
import fr.cedricalibert.hibernate.demo.entity.InstructorDetail;
import fr.cedricalibert.hibernate.demo.entity.Review;
import fr.cedricalibert.hibernate.demo.entity.Student;

public class CreateCourseAndStudentsDemo {

	public static void main(String[] args) {
		//create session factory 
		SessionFactory factory = new Configuration()
								.configure("hibernate.cfg.xml")
								.addAnnotatedClass(Instructor.class)
								.addAnnotatedClass(InstructorDetail.class)
								.addAnnotatedClass(Course.class)
								.addAnnotatedClass(Review.class)
								.addAnnotatedClass(Student.class)
								.buildSessionFactory();
		
		//create session 
		Session session = factory.getCurrentSession();
		
		try {
			
			//start a transaction
			session.beginTransaction();
			
			//create a course
			Course course = new Course("Course 1");
			
			session.save(course);
			
			Student strudent1 = new Student("Cédric", "ALIBERT", "test@test.fr");
			Student strudent2 =new Student("Cédric2", "ALIBERT2", "test2@test.fr");
			Student strudent3 =new Student("Cédric3", "ALIBERT3", "test3@test.fr");
			
			//add some students
			course.addStudent(strudent1);
			course.addStudent(strudent2);
			course.addStudent(strudent3);
			
			//save the student
			session.save(strudent1);
			session.save(strudent2);
			session.save(strudent3);
			
			System.out.println("Student course : "+course.getStudents());
			
			//commit transaction
			session.getTransaction().commit();
		
			
			System.out.println("Done");
			
		}
		finally {
			session.close();
			factory.close();
		}

	}

}
