package fr.cedricalibert.hibernate.demo;


import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import fr.cedricalibert.hibernate.demo.entity.Course;
import fr.cedricalibert.hibernate.demo.entity.Instructor;
import fr.cedricalibert.hibernate.demo.entity.InstructorDetail;
import fr.cedricalibert.hibernate.demo.entity.Review;
import fr.cedricalibert.hibernate.demo.entity.Student;

public class AddCoursesForStudentDemo {

	public static void main(String[] args) {
		//create session factory 
		SessionFactory factory = new Configuration()
								.configure("hibernate.cfg.xml")
								.addAnnotatedClass(Instructor.class)
								.addAnnotatedClass(InstructorDetail.class)
								.addAnnotatedClass(Course.class)
								.addAnnotatedClass(Review.class)
								.addAnnotatedClass(Student.class)
								.buildSessionFactory();
		
		//create session 
		Session session = factory.getCurrentSession();
		
		try {
			
			//start a transaction
			session.beginTransaction();
			
			int id = 2;
			Student student = session.get(Student.class, id);
			
			System.out.println("Courses : "+student.getCourses());
			
			System.out.println("Create Courses ");
			Course course1 = new Course("course 2");
			Course course2 = new Course("course 3");
			Course course3 = new Course("course 4");
			
			System.out.println("Add student to courses");
			course1.addStudent(student);
			course2.addStudent(student);
			course3.addStudent(student);
	
			session.save(course1);
			session.save(course2);
			session.save(course3);
			
			//commit transaction
			session.getTransaction().commit();
		
			System.out.println("Courses : "+student.getCourses());
			
			System.out.println("Done");
			
		}
		finally {
			session.close();
			factory.close();
		}

	}

}
