package fr.cedricalibert.hibernate.demo;


import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import fr.cedricalibert.hibernate.demo.entity.Course;
import fr.cedricalibert.hibernate.demo.entity.Instructor;
import fr.cedricalibert.hibernate.demo.entity.InstructorDetail;
import fr.cedricalibert.hibernate.demo.entity.Review;
import fr.cedricalibert.hibernate.demo.entity.Student;

public class GetCoursesForStudentDemo {

	public static void main(String[] args) {
		//create session factory 
		SessionFactory factory = new Configuration()
								.configure("hibernate.cfg.xml")
								.addAnnotatedClass(Instructor.class)
								.addAnnotatedClass(InstructorDetail.class)
								.addAnnotatedClass(Course.class)
								.addAnnotatedClass(Review.class)
								.addAnnotatedClass(Student.class)
								.buildSessionFactory();
		
		//create session 
		Session session = factory.getCurrentSession();
		
		try {
			
			//start a transaction
			session.beginTransaction();
			
			int id = 2;
			Student student = session.get(Student.class, id);
			
			System.out.println("Student : "+ student);
			System.out.println("Courses : "+student.getCourses());
			
			//commit transaction
			session.getTransaction().commit();
			
			System.out.println("Done");
			
		}
		finally {
			session.close();
			factory.close();
		}

	}

}
